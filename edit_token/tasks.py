from celery import shared_task
from django.apps import apps
from django.utils import timezone


@shared_task(bind=True)
def remove_old_edit_tokens(self):
    """
    Remove old EditTokens
    :param self: Task
    :return: void
    """
    EditToken = apps.get_model('edit_token', 'EditToken')
    now = timezone.now()
    EditToken.objects.filter(active_before__lt=now).delete()


@shared_task(bind=True)
def remove_old_per_model_edit_tokens(self):
    """
    Remove old PerModelEditTokens
    :param self: Task
    :return: void
    """
    PerModelEditToken = apps.get_model('edit_token', 'PerModelEditToken')
    now = timezone.now()
    PerModelEditToken.objects.filter(active_before__lt=now).delete()
