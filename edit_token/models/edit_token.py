from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.conf import settings
from django.utils import timezone
from datetime import timedelta
import uuid


def get_edit_token_active_before():
    """
    Build EditToken.active_before default value
    :return: datetime
    """
    now = timezone.now()
    return now + timedelta(minutes=settings.EDITTOKEN_MAX_LIVE_MINUTES)


class EditTokenManager(models.Manager):
    """
    Custom EditToken manager
    """

    def get_active_token(self, target, owner, edit_type='base_instance', custom_live_minutes=None):
        """
        Get active token to change target instance
        :param target: Model instance
        :param owner: User
        :param edit_type: str
        :param custom_live_minutes: Int or None
        :return: EditToken or None
        """
        now = timezone.now()
        content_type = ContentType.objects.get_for_model(type(target))
        token_settings_model = settings.EDITTOKEN_LIMITS.get(type(target).__name__, {})
        token_settings = token_settings_model.get(edit_type, None)
        if token_settings is not None:
            qs = self.filter(content_type=content_type, object_id=target.id, date_created__lte=now,
                             active_before__gt=now, edit_type=edit_type)
            max_uses = token_settings.get('total_uses', 0)
            if max_uses > 0:
                qs2 = qs.filter(total_uses__lt=max_uses)
                if qs.exists() and qs2.exists():
                    return qs2.first()
                elif not qs.exists():
                    live_minutes = custom_live_minutes or token_settings.get(
                        'max_live_minutes', settings.EDITTOKEN_MAX_LIVE_MINUTES)
                    new_token = self.create(
                        content_object=target,
                        owner=owner,
                        active_before=now + timedelta(minutes= live_minutes),
                        edit_type=edit_type
                    )
                    return new_token
        return None


class EditToken(models.Model):
    """
    Token to access to change models
    """
    owner = models.ForeignKey(
        'users.User',
        verbose_name=_('owner'),
        related_name='owned_edit_tokens',
        on_delete=models.CASCADE
    )
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey(
        'content_type',
        'object_id'
    )
    date_created = models.DateTimeField(
        verbose_name=_('date created'),
        blank=True,
        auto_now_add=True
    )
    active_before = models.DateTimeField(
        verbose_name=_('date created'),
        default=get_edit_token_active_before
    )
    total_uses = models.PositiveIntegerField(
        verbose_name=_('total uses'),
        default=0
    )
    edit_type = models.CharField(
        verbose_name=_('edit type'),
        max_length=255,
        default='base'
    )
    uid = models.UUIDField(
        verbose_name=_('uuid'),
        default=uuid.uuid4,
        unique=True,
        blank=True
    )

    class Meta:
        verbose_name = _('edit token')
        verbose_name_plural = _('edit_tokens')
        ordering = ['date_created']

    def __str__(self):
        return f'{self.owner}: {self.date_created} - {self.active_before}. {self.total_uses} | {self.content_object}'

    objects = EditTokenManager()


__all__ = ['EditToken']
