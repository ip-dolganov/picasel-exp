from rest_framework import serializers
from django.apps import apps


EditToken = apps.get_model('edit_token', 'EditToken')


class EditTokenViewSerializer(serializers.ModelSerializer):
    """
    SerializeEditToken to view
    """

    class Meta:
        model = EditToken
        fields = ['uid']


__all__ = ['EditTokenViewSerializer']
