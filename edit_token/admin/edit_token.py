from django.contrib import admin


class EditTokenAdmin(admin.ModelAdmin):
    """
    EditToken Admin
    """
    fields = ['owner', 'content_type', 'object_id', 'active_before', 'total_uses', 'edit_type']
    list_display = ['__str__', 'uid']
