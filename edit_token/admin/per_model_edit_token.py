from django.contrib import admin


class PerModelEditTokenAdmin(admin.ModelAdmin):
    """
    PerModelEditToken Admin
    """
    fields = ['owner', 'model_name', 'active_before', 'total_uses', 'edit_type']
    list_display = ['__str__']
