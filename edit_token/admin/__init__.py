from django.contrib import admin
from django.apps import apps
from edit_token.admin.edit_token import EditTokenAdmin
from edit_token.admin.per_model_edit_token import PerModelEditTokenAdmin

PerModelEditToken = apps.get_model('edit_token', 'PerModelEditToken')
EditToken = apps.get_model('edit_token', 'EditToken')


admin.site.register(EditToken, EditTokenAdmin)
admin.site.register(PerModelEditToken, PerModelEditTokenAdmin)
