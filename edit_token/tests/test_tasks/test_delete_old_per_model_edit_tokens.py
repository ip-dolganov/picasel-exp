from django.test import TestCase, override_settings
from django.utils import timezone
from unittest.mock import patch
from datetime import datetime
from django.apps import apps
from model_mommy import mommy
from edit_token.tasks import remove_old_per_model_edit_tokens

PerModelEditToken = apps.get_model('edit_token', 'PerModelEditToken')
EDITTOKEN_LIMITS = {
    'User': {
        'base': {
            'max_live_minutes': 1, # 1 minute
            'total_uses': 5
        },
        'base_instance': {
            'max_live_minutes': 1,  # 1 minute
            'total_uses': 1
        }
    }
}

@override_settings(EDITTOKEN_LIMITS=EDITTOKEN_LIMITS, CELERY_TASK_ALWAYS_EAGER=True)
class TestRemoveOldPerModelEditTokens(TestCase):
    """
    Test remove_old_edit_tokens task
    """

    def setUp(self):
        self.admin1 = mommy.make_recipe('users.admin1')
        self.admin2 = mommy.make_recipe('users.admin2')

    def test_users_edit_token(self):
        creation_time = timezone.make_aware(datetime(2019, 1, 29, 20, 15, 10))
        valid_time = timezone.make_aware(datetime(2019, 1, 29, 20, 15, 15))
        off_time = timezone.make_aware(datetime(2019, 1, 29, 20, 16, 11))
        with patch('django.utils.timezone.now', return_value=creation_time):
            to_admin1_token = PerModelEditToken.objects.get_active_token('User', self.admin1)
            self.assertIsNotNone(to_admin1_token)
        with patch('django.utils.timezone.now', return_value=valid_time):
            self.assertTrue(PerModelEditToken.objects.all().exists())
            remove_old_per_model_edit_tokens.delay()
            self.assertTrue(PerModelEditToken.objects.all().exists())
        with patch('django.utils.timezone.now', return_value=off_time):
            self.assertTrue(PerModelEditToken.objects.all().exists())
            remove_old_per_model_edit_tokens.delay()
            self.assertFalse(PerModelEditToken.objects.all().exists())
