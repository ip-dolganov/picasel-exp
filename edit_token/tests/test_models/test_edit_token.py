from django.test import TestCase, override_settings
from django.utils import timezone
from unittest.mock import patch
from datetime import datetime
from django.apps import apps
from model_mommy import mommy

EditToken = apps.get_model('edit_token', 'EditToken')
EDITTOKEN_LIMITS = {
    'User': {
        'base': {
            'max_live_minutes': 1, # 1 minute
            'total_uses': 5
        },
        'base_instance': {
            'max_live_minutes': 1,  # 1 minute
            'total_uses': 1
        }
    }
}

@override_settings(EDITTOKEN_LIMITS=EDITTOKEN_LIMITS)
class TestEditToken(TestCase):
    """
    Test EditToken model
    """

    def setUp(self):
        self.admin1 = mommy.make_recipe('users.admin1')
        self.admin2 = mommy.make_recipe('users.admin2')

    def test_users_edit_token(self):
        creation_time = timezone.make_aware(datetime(2019, 1, 29, 20, 15, 10))
        valid_time = timezone.make_aware(datetime(2019, 1, 29, 20, 15, 15))
        off_time = timezone.make_aware(datetime(2019, 1, 29, 20, 16, 11))
        with patch('django.utils.timezone.now', return_value=creation_time):
            to_admin1_token = EditToken.objects.get_active_token(self.admin1, self.admin1)
            self.assertIsNotNone(to_admin1_token)
        with patch('django.utils.timezone.now', return_value=valid_time):
            to_admin1_token_copy = EditToken.objects.get_active_token(self.admin1, self.admin1)
            self.assertEqual(to_admin1_token, to_admin1_token_copy)
        with patch('django.utils.timezone.now', return_value=off_time):
            to_admin1_token_new = EditToken.objects.get_active_token(self.admin1, self.admin1)
            self.assertNotEqual(to_admin1_token, to_admin1_token_new)
            self.assertIsNotNone(to_admin1_token_new)
        with patch('django.utils.timezone.now', return_value=valid_time):
            to_admin1_token.total_uses = 5
            to_admin1_token.save()
            to_admin1_token_new1 = EditToken.objects.get_active_token(self.admin1, self.admin1)
            self.assertNotEqual(to_admin1_token, to_admin1_token_new1)
            self.assertIsNone(to_admin1_token_new1)
