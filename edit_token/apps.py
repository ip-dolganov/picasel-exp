from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class EditTokenConfig(AppConfig):
    """
    Configure edit_token
    """

    name = 'edit_token'
    verbose_name = _('edit token')

    def ready(self):
        super().ready()


__all__ = ['EditTokenConfig']
