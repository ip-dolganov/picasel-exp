from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserBaseSerializer(serializers.ModelSerializer):
    """
    Serialize user's base info
    """

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'patronymic', 'last_name', 'is_staff', 'is_superuser']


__all__ = ['UserBaseSerializer']
