from django.urls import reverse
from django.contrib.auth import get_user_model
from model_mommy import mommy
from core.utils.tests import APITestCaseWithContentTypesClearing
from rest_framework import status
from urllib.parse import urlencode
from django.test import override_settings
from django.apps import apps

User = get_user_model()
Task = apps.get_model('tasks_system', 'Task')

EDITTOKEN_LIMITS = {
    'User': {
        'base': {
            'max_live_minutes': 1, # 1 minute
            'total_uses': 1
        }
    }
}

@override_settings(EDITTOKEN_LIMITS=EDITTOKEN_LIMITS)
class TestUserViewSet(APITestCaseWithContentTypesClearing):
    """
    Test UserViewSet
    """

    url = reverse('api:users:user-list')

    def setUp(self):
        self.admin1 = mommy.make_recipe('users.admin1')
        self.admin1.set_password('qwerty')
        self.admin1.save()
        self.admin2 = mommy.make_recipe('users.admin2')

    def test_list_anonymously(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_authenticated(self):
        auth_client = self._get_auth_client()
        response = auth_client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

    def test_list_filter_full_name(self):
        auth_client = self._get_auth_client()
        url = self.url + '?' + urlencode({'full_name': 'SEC'})
        response = auth_client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['id'], self.admin2.id)

    def test_list_filter_has_tasks(self):
        Task.objects.create(author=self.admin1, assignee=self.admin2, title='task')
        auth_client = self._get_auth_client()
        url = self.url + '?' + urlencode({'has_tasks': True})
        response = auth_client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['id'], self.admin2.id)

    def test_retrieve_anonymously(self):
        url = reverse('api:users:user-detail', kwargs={'pk': self.admin2.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_retrieve_authenticated(self):
        auth_client = self._get_auth_client()
        url = reverse('api:users:user-detail', kwargs={'pk': self.admin2.id})
        response = auth_client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_anonymously(self):
        url = reverse('api:users:user-detail', kwargs={'pk': self.admin2.id})
        data = {
            'last_name': 'new lastname'
        }
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_authenticated(self):
        auth_client = self._get_auth_client()
        url = reverse('api:users:user-detail', kwargs={'pk': self.admin2.id})
        data = {
            'last_name': 'new lastname'
        }
        response = auth_client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_anonymously(self):
        url = reverse('api:users:user-detail', kwargs={'pk': self.admin2.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_authenticated(self):
        auth_client = self._get_auth_client()
        url = reverse('api:users:user-detail', kwargs={'pk': self.admin2.id})
        response = auth_client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update_limit(self):
        auth_client = self._get_auth_client()
        url = reverse('api:users:user-detail', kwargs={'pk': self.admin2.id})
        data = {
            'last_name': 'new lastname'
        }
        response = auth_client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {
            'last_name': 'new lastname 2'
        }
        response = auth_client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
