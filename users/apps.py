from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    """
    Configure users
    """

    name = 'users'
    verbose_name = _('Users')

    def ready(self):
        super().ready()


__all__ = ['UsersConfig']
