from django.urls import re_path
from users.views import SignInAPIView, LogOutAPIView, UserViewSet
from rest_framework.routers import DefaultRouter

app_name = 'users'

router = DefaultRouter()
router.register(r'', UserViewSet)


urlpatterns = [
    re_path(r'sign-in/', SignInAPIView.as_view(), name='sign_in'),
    re_path(r'logout/', LogOutAPIView.as_view(), name='logout'),
] + router.urls
