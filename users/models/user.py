from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

class User(AbstractUser):
    """
    Custom User model
    """

    patronymic = models.CharField(
        verbose_name=_('Patronymic'),
        max_length=255,
        blank=True,
        null=True
    )

    class Meta(AbstractUser.Meta):
        abstract = False
        ordering = ['date_joined']

    def __str__(self):
        return self.username


__all__ = ['User']
