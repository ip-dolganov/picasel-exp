from django.apps import apps
from django.contrib.auth import login, logout
from rest_framework import views
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from users.serializers import SignInSerializer, UserAuthSerializer
from users.views.permissions import UnauthorizedOnly

Token = apps.get_model('authtoken', 'Token')


class SignInAPIView(views.APIView):
    """
    Sign-In API View
    """

    serializer_class = SignInSerializer
    permission_classes = [UnauthorizedOnly]

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.user
            login(request, user)
            if not hasattr(user, 'auth_token'):
                Token.objects.create(user=user)
            resp_serializer = UserAuthSerializer(user)
            return Response(resp_serializer.data)


class LogOutAPIView(views.APIView):
    """
    Logout API View
    """
    permission_classes = [AllowAny]

    def get(self, request):
        logout(request)
        return Response({'logout': True})