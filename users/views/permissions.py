from rest_framework import permissions


class UnauthorizedOnly(permissions.BasePermission):
    """
    Allow access for unauthenticated users only
    """

    def has_permission(self, request, view):
        return not request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return not request.user.is_authenticated
