from django.contrib.auth import get_user_model
from rest_framework import permissions, mixins, viewsets
from users.serializers import UserBaseSerializer, UserUpdateSerializer
from core.utils.permissions import PerModelEditTokenPermissions
from django_filters import rest_framework as filters
from core.utils.view_set import ViewSetMixin
from django.db.models import Q
from django.apps import apps
from rest_framework import status
from rest_framework.response import Response

User = get_user_model()
PerModelEditToken = apps.get_model('edit_token', 'PerModelEditToken')


class UserViewSetFilter(filters.FilterSet):
    """
    Custom filter for the User ViewSet
    """

    full_name = filters.CharFilter(method='filter_by_full_name')
    has_tasks = filters.BooleanFilter(field_name='assignee_tasks', lookup_expr='isnull', exclude=True)

    ordering = filters.OrderingFilter(
        fields=(
            ('date_joined', 'date_joined'),
        )
    )

    def filter_by_full_name(self, qs, name, value):
        if value is not None and len(value) > 0:
            if len(value) < 3:
                return qs.filter(
                    Q(first_name__istartswith=value) | Q(last_name__istartswith=value) | Q(
                        patronymic__istartswith=value))
            else:
                return qs.filter(
                    Q(first_name__icontains=value) | Q(last_name__icontains=value) | Q(
                        patronymic__icontains=value))
        return qs

    def __init__(self, data=None, *args, **kwargs):
        del (kwargs['action'])
        super().__init__(data, *args, **kwargs)

    class Meta:
        model = User
        fields = ('ordering', 'full_name')


class UserViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                  ViewSetMixin, viewsets.GenericViewSet):
    """
    User ViewSet
    """

    serializer_classes = {
        'list': UserBaseSerializer,
        'retrieve': UserBaseSerializer,
        'update': UserUpdateSerializer,
        'partial_update': UserUpdateSerializer,
        'destroy': UserBaseSerializer
    }
    permissions_map = {
        'list': [permissions.IsAuthenticated],
        'retrieve': [permissions.IsAuthenticated],
        'update': [permissions.IsAuthenticated, PerModelEditTokenPermissions],
        'partial_update': [permissions.IsAuthenticated, PerModelEditTokenPermissions],
        'destroy': [permissions.IsAuthenticated, PerModelEditTokenPermissions],
    }
    queryset = User.objects.all().distinct()
    filter_class = UserViewSetFilter

    @staticmethod
    def _update_edit_token(instance, request):
        """
        Update PerModelEditToken
        :param instance: User
        :param request: Request
        :return: void
        """
        edit_token = PerModelEditToken.objects.get_active_token('User', request.user)
        edit_token.total_uses += 1
        edit_token.save()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        # Need to change to token without instance relation
        self._update_edit_token(instance, request)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        self._update_edit_token(instance, request)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


__all__ = ['UserViewSet']
