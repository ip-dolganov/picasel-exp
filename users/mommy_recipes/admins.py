from model_mommy.recipe import Recipe
from django.contrib.auth import get_user_model

User = get_user_model()

admin1 = Recipe(
    User,
    username='Admin1',
    email='admin1@test.com',
    first_name='Admin',
    last_name='The First'
)

admin2 = Recipe(
    User,
    username='Admin2',
    email='admin2@test.com',
    first_name='Admin',
    last_name='The Second'
)

__all__ = ['admin1', 'admin2']
