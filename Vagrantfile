# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  config.vm.provider "virtualbox" do |vb|
    # Don't boot with headless mode
    # vb.gui = true

    # Use VBoxManage to customize the VM. For example to change memory:
    # vb.customize ["modifyvm", :id, "--memory", "1024"]
    vb.memory = 2048
    vb.cpus = 2
    config.vm.synced_folder ".", "/vagrant", type: "virtualbox"
  end

  config.vm.define "project", primary: true, autorestart: false do |project|
    project.vm.box = "debian/stretch64"
    project.vm.network "private_network", ip: "172.16.99.105"
    project.vm.network "forwarded_port", guest: 22, host: 2241, id: "ssh"

    project.vm.provision "project", type: "shell" do |shell|
      shell.path = "provision/install_ansible.sh"
      shell.privileged = false
      shell.keep_color = true
    end

    project.vm.provision "ansible_local" do |ansible|
        ansible.inventory_path  = "./provision/hosts"
        ansible.limit           = "local"
        ansible.playbook        = "provision/vagrant.yml"
        ansible.verbose         = 'vvvv'
    end

    project.vm.provision "ansible_local", run: "always" do |ansible|
        ansible.inventory_path  = "./provision/hosts"
        ansible.limit           = "local"
        ansible.playbook        = "provision/local_always.yml"
        ansible.verbose         = 'vvvv'
    end

    project.vm.box_check_update = false
  end
end
