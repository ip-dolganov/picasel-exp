from tasks_system.views import TaskViewSet
from rest_framework.routers import DefaultRouter

app_name = 'tasks'

router = DefaultRouter()
router.register(r'', TaskViewSet)


urlpatterns = router.urls
