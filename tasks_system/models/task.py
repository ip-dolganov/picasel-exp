from django.db import models
from django.utils.translation import ugettext_lazy as _


class Task(models.Model):
    """
    Task
    """
    title = models.CharField(
        verbose_name=_('title'),
        max_length=1000
    )
    author = models.ForeignKey(
        'users.User',
        verbose_name=_('author'),
        related_name='owned_tasks',
        on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        'users.User',
        verbose_name=_('author'),
        related_name='assignee_tasks',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )
    date_created = models.DateTimeField(
        verbose_name=_('date created'),
        blank=True,
        auto_now_add=True
    )

    class Meta:
        verbose_name = _('task')
        verbose_name_plural = _('tasks')
        ordering = ['date_created']

    def __str__(self):
        return f'{self.title}: {self.date_created}'


__all__ = ['Task']
