from django.urls import reverse
from django.apps import apps
from model_mommy import mommy
from core.utils.tests import APITestCaseWithContentTypesClearing
from rest_framework import status
from urllib.parse import urlencode
from django.test import override_settings

Task = apps.get_model('tasks_system', 'Task')

EDITTOKEN_LIMITS = {
    'User': {
        'base': {
            'max_live_minutes': 1,  # 1 minute
            'total_uses': 1
        }
    },
    'Task': {
        'base': {
            'max_live_minutes': 1,  # 1 minute
            'total_uses': 1
        },
        'base_instance': {
            'max_live_minutes': 1,  # 1 minute
            'total_uses': 1
        }
    }
}


@override_settings(EDITTOKEN_LIMITS=EDITTOKEN_LIMITS)
class TestTaskViewSet(APITestCaseWithContentTypesClearing):
    """
    Test TaskViewSet
    """

    url = reverse('api:tasks:task-list')

    def setUp(self):
        self.admin1 = mommy.make_recipe('users.admin1')
        self.admin1.set_password('qwerty')
        self.admin1.save()
        self.admin2 = mommy.make_recipe('users.admin2')
        self.task1 = Task.objects.create(author=self.admin1, title='Buy Pizza')
        self.task2 = Task.objects.create(author=self.admin2, title='Buy Cheesecake')

    def test_list_anonymously(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_list_authenticated(self):
        auth_client = self._get_auth_client()
        response = auth_client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 2)

    def test_list_filter_full_name(self):
        auth_client = self._get_auth_client()
        url = self.url + '?' + urlencode({'title': 'CaKe'})
        response = auth_client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['id'], self.task2.id)

    def test_list_filter_assignee(self):
        task = Task.objects.create(author=self.admin2, title='Buy Bread', assignee=self.admin1)
        auth_client = self._get_auth_client()
        url = self.url + '?' + urlencode({'assignee': self.admin1.id})
        response = auth_client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['id'], task.id)

    def test_retrieve_anonymously(self):
        url = reverse('api:tasks:task-detail', kwargs={'pk': self.task1.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_retrieve_authenticated(self):
        auth_client = self._get_auth_client()
        url = reverse('api:tasks:task-detail', kwargs={'pk': self.task2.id})
        response = auth_client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_anonymously(self):
        url = reverse('api:tasks:task-detail', kwargs={'pk': self.task2.id})
        data = {
            'title': 'new title'
        }
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_authenticated(self):
        auth_client = self._get_auth_client()
        url = reverse('api:tasks:task-detail', kwargs={'pk': self.task2.id})
        data = {
            'title': 'new title'
        }
        response = auth_client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_anonymously(self):
        url = reverse('api:tasks:task-detail', kwargs={'pk': self.task2.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_authenticated(self):
        auth_client = self._get_auth_client()
        url = reverse('api:tasks:task-detail', kwargs={'pk': self.task2.id})
        response = auth_client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update_limit(self):
        auth_client = self._get_auth_client()
        url = reverse('api:tasks:task-detail', kwargs={'pk': self.task2.id})
        data = {
            'title': 'new title'
        }
        response = auth_client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {
            'title': 'new title 2'
        }
        response = auth_client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_edit_token_anonymously(self):
        url = reverse('api:tasks:task-get_edit_token', kwargs={'pk': self.task1.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_edit_token_authenticated(self):
        auth_client = self._get_auth_client()
        url = reverse('api:tasks:task-get_edit_token', kwargs={'pk': self.task2.id})
        response = auth_client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('uid', response.data.keys())

    def test_update_assignee_anonymously(self):
        url = reverse('api:tasks:task-update_assignee', kwargs={'pk': self.task2.id})
        data = {
            'assignee': self.admin2.id
        }
        response = self.client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_assignee_without_token(self):
        auth_client = self._get_auth_client()
        url = reverse('api:tasks:task-update_assignee', kwargs={'pk': self.task2.id})
        data = {
            'assignee': self.admin2.id
        }
        response = auth_client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_assignee_valid(self):
        auth_client = self._get_auth_client()
        url = reverse('api:tasks:task-get_edit_token', kwargs={'pk': self.task2.id})
        response = auth_client.get(url)
        credentials = auth_client._credentials
        credentials['HTTP_EDIT_TOKEN'] = response.data['uid']
        auth_client.credentials(**credentials)
        url = reverse('api:tasks:task-update_assignee', kwargs={'pk': self.task2.id})
        data = {
            'assignee': self.admin2.id
        }
        response = auth_client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_assignee_limit(self):
        auth_client = self._get_auth_client()
        url = reverse('api:tasks:task-get_edit_token', kwargs={'pk': self.task2.id})
        response = auth_client.get(url)
        credentials = auth_client._credentials
        credentials['HTTP_EDIT_TOKEN'] = response.data['uid']
        auth_client.credentials(**credentials)
        url = reverse('api:tasks:task-update_assignee', kwargs={'pk': self.task2.id})
        data = {
            'assignee': self.admin2.id
        }
        response = auth_client.patch(url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {
            'assignee': None
        }
        response = auth_client.patch(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_anonymously(self):
        data = {
            'title': 'title'
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_authenticated(self):
        auth_client = self._get_auth_client()
        data = {
            'title': 'title'
        }
        response = auth_client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_limit(self):
        auth_client = self._get_auth_client()
        data = {
            'title': 'title'
        }
        response = auth_client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = {
            'title': 'another title'
        }
        response = auth_client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
