from django.test import TestCase
from model_mommy import mommy
from django.apps import apps

Task = apps.get_model('tasks_system', 'Task')



class TestTaskModel(TestCase):
    """
    Test Task Model
    """

    def setUp(self):
        self.admin1 = mommy.make_recipe('users.admin1')
        self.admin2 = mommy.make_recipe('users.admin2')


    def test_crud(self):
        self.assertFalse(Task.objects.all().exists())
        task = Task.objects.create(author=self.admin1, title='Title1')
        self.assertTrue(Task.objects.all().exists())
        task.delete()
        self.assertFalse(Task.objects.all().exists())
