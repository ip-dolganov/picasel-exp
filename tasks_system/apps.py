from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class TaskSystemConfig(AppConfig):
    """
    Configure edit_token
    """

    name = 'tasks_system'
    verbose_name = _('tasks system')

    def ready(self):
        super().ready()


__all__ = ['TaskSystemConfig']
