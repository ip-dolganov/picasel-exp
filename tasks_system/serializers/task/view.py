from rest_framework import serializers
from users.serializers.user.view import UserBaseSerializer
from django.apps import apps

Task = apps.get_model('tasks_system', 'Task')


class TaskViewSerializer(serializers.ModelSerializer):
    """
    Serialize Task to View
    """
    author = UserBaseSerializer()
    assignee = UserBaseSerializer()

    class Meta:
        model = Task
        fields = ['id', 'title', 'author', 'assignee', 'date_created']


__all__ = ['TaskViewSerializer']
