from rest_framework import serializers
from django.apps import apps
from tasks_system.serializers.task.view import TaskViewSerializer

Task = apps.get_model('tasks_system', 'Task')


class TaskUpdateBaseSerializer(serializers.ModelSerializer):
    """
    Serialize Task to Update (base)
    """

    @property
    def data(self):
        serializer = TaskViewSerializer(self.instance, context=self.context)
        return serializer.data

    class Meta:
        model = Task
        fields = ['title']


class TaskUpdateAssigneeSerializer(serializers.ModelSerializer):
    """
    Serialize Task to Update (base)
    """

    @property
    def data(self):
        serializer = TaskViewSerializer(self.instance, context=self.context)
        return serializer.data

    class Meta:
        model = Task
        fields = ['assignee']


class TaskCreateSerializer(serializers.ModelSerializer):
    """
    Serialize Task to Update (base)
    """

    def validate(self, attrs):
        attrs['author'] = self.context['request'].user
        return attrs

    @property
    def data(self):
        serializer = TaskViewSerializer(self.instance, context=self.context)
        return serializer.data

    class Meta:
        model = Task
        fields = ['title', 'assignee']


__all__ = ['TaskUpdateBaseSerializer', 'TaskUpdateAssigneeSerializer', 'TaskCreateSerializer']
