from rest_framework import permissions, mixins, viewsets
from tasks_system.serializers.task import TaskViewSerializer, TaskUpdateBaseSerializer, TaskUpdateAssigneeSerializer, \
    TaskCreateSerializer
from edit_token.serializers import EditTokenViewSerializer
from core.utils.permissions import PerModelEditTokenPermissions, EditTokenPermissions
from django_filters import rest_framework as filters
from core.utils.view_set import ViewSetMixin
from django.apps import apps
from rest_framework.response import Response
from rest_framework.decorators import action
from django.contrib.auth import get_user_model

Task = apps.get_model('tasks_system', 'Task')
PerModelEditToken = apps.get_model('edit_token', 'PerModelEditToken')
EditToken = apps.get_model('edit_token', 'EditToken')
User = get_user_model()


class TaskViewSetFilter(filters.FilterSet):
    """
    Custom filter for the Task ViewSet
    """

    title = filters.CharFilter(method='filter_by_title')
    assignee = filters.ModelChoiceFilter(queryset=User.objects.all(), field_name='assignee')

    def filter_by_title(self, qs, name, value):
        if value is not None and len(value) > 0:
            if len(value) < 3:
                return qs.filter(title__istartswith=value)
            else:
                return qs.filter(title__icontains=value)
        return qs

    ordering = filters.OrderingFilter(
        fields=(
            ('date_created', 'date_created'),
        )
    )

    def __init__(self, data=None, *args, **kwargs):
        del (kwargs['action'])
        super().__init__(data, *args, **kwargs)

    class Meta:
        model = Task
        fields = ('ordering', 'title', 'assignee')


class TaskViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                  mixins.CreateModelMixin, ViewSetMixin, viewsets.GenericViewSet):
    """
    Task ViewSet
    """

    serializer_classes = {
        'list': TaskViewSerializer,
        'retrieve': TaskViewSerializer,
        'update': TaskUpdateBaseSerializer,
        'partial_update': TaskUpdateBaseSerializer,
        'destroy': TaskUpdateBaseSerializer,
        'get_edit_token': EditTokenViewSerializer,
        'update_assignee': TaskUpdateAssigneeSerializer,
        'create': TaskCreateSerializer,
    }
    permissions_map = {
        'list': [permissions.IsAuthenticated],
        'retrieve': [permissions.IsAuthenticated],
        'update': [permissions.IsAuthenticated, PerModelEditTokenPermissions],
        'partial_update': [permissions.IsAuthenticated, PerModelEditTokenPermissions],
        'destroy': [permissions.IsAuthenticated, PerModelEditTokenPermissions],
        'get_edit_token': [permissions.IsAuthenticated],
        'update_assignee': [permissions.IsAuthenticated, EditTokenPermissions],
        'create': [permissions.IsAuthenticated, PerModelEditTokenPermissions],
    }
    queryset = Task.objects.all().distinct()
    filter_class = TaskViewSetFilter

    @staticmethod
    def _update_per_model_edit_token(request):
        """
        Update PerModelEditToken
        :param request: Request
        :return: void
        """
        edit_token = PerModelEditToken.objects.get_active_token('Task', request.user)
        edit_token.total_uses += 1
        edit_token.save()

    @staticmethod
    def _update_edit_token(instance, request):
        """
        Update PerModelEditToken
        :param instance: Task
        :param request: Request
        :return: void
        """
        edit_token = EditToken.objects.get_active_token(instance, request.user)
        edit_token.total_uses += 1
        edit_token.save()

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        self._update_per_model_edit_token(request)
        return response

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        use_edit_token = kwargs.pop('use_edit_token', False)
        if not use_edit_token:
            self._update_per_model_edit_token(request)
        else:
            self._update_edit_token(instance, request)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        self._update_per_model_edit_token(request)
        return response

    @action(methods=['get'], detail=True, url_path='get-edit-token', url_name='get_edit_token')
    def get_edit_token(self, request, **kwargs):
        """
        Returns EditToken
        :param request: Request
        :param kwargs: dict or None
        :return: Response
        """
        instance = self.get_object()
        token = EditToken.objects.get_active_token(instance, request.user)
        serializer = self.get_serializer(token)
        return Response(serializer.data)

    @action(methods=['patch', 'put'], detail=True, url_path='update-assignee', url_name='update_assignee')
    def update_assignee(self, request, **kwargs):
        """
        Update assignee
        :param request: Request
        :param kwargs: dict or None
        :return: Response
        """
        kwargs['use_edit_token'] = True
        return self.update(request, **kwargs)


__all__ = ['TaskViewSet']
