from django.contrib import admin
from django.apps import apps
from tasks_system.admin.task import TaskAdmin

Task = apps.get_model('tasks_system', 'Task')


admin.site.register(Task, TaskAdmin)
