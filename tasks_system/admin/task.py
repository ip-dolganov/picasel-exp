from django.contrib import admin


class TaskAdmin(admin.ModelAdmin):
    """
    Task Admin interface
    """

    fields = ['title', 'author', 'assignee']
