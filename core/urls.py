from django.contrib import admin
from django.contrib.staticfiles import views as static_views
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.documentation import include_docs_urls
from rest_framework import authentication, permissions
from django.urls import include, re_path

urlpatterns = [
    re_path(r'^{0}(?P<path>.*)$'.format(settings.STATIC_URL[1:]), static_views.serve),
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^api/', include('core.urls_api', namespace='api')),
    re_path(r'^api/docs/', include_docs_urls(title='Pidruchnik API',
                                             authentication_classes=[authentication.SessionAuthentication,
                                                                     authentication.TokenAuthentication],
                                             permission_classes=[permissions.IsAdminUser])),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
