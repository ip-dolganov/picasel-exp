from core.utils.pagination import PicaselPagination
from core.utils.filter_backend import WithActionFilterBackend


class ViewSetMixin(object):
    """
    ViewSet Mixin
    """

    @property
    def serializer_classes(self):
        """
        Serializer classes per action
        :return: dict
        """
        raise NotImplementedError

    @property
    def permissions_map(self):
        """
        Permission classes per action
        :return: dict
        """
        raise NotImplementedError

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.serializer_classes['list'])

    def get_permissions(self):
        permissions_ = self.permissions_map.get(self.action, self.permissions_map['list'])
        return [permission() for permission in permissions_]

    pagination_class = PicaselPagination
    filter_backends = [WithActionFilterBackend]
