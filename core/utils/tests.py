from rest_framework.test import APITestCase
from django.test import TestCase, override_settings
from django.apps import apps
import os
from django.conf import settings
from shutil import rmtree
from copy import deepcopy
from django.urls import reverse


@override_settings(DISABLE_OLD_APPS=False)
class APITestCaseWithContentTypesClearing(APITestCase):
    """
    Clear ContentTypes before the testing
    """

    sign_in_url = reverse('api:users:sign_in')

    def _get_auth_client(self, user='admin1', password='qwerty'):
        """
        Get client with authentication credentials
        :param user: str
        :param password: str - Password
        :return: Client
        """
        auth_client = deepcopy(self.client)
        response = auth_client.post(self.sign_in_url, data={'login': user, 'password': password})
        auth_client.credentials(HTTP_AUTHORIZATION='Token ' + response.data['token'])
        return auth_client


class TestCaseWithContentTypesClearing(TestCase):
    """
    Clear ContentTypes before the testing
    """

    @classmethod
    def setUpClass(cls):
        content_type = apps.get_model('contenttypes', 'ContentType')
        content_type.objects.all().delete()
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        for root, dirs, files in os.walk(settings.TEST_MEDIA_ROOT):
            for name in dirs:
                rmtree(os.path.join(root, name))
