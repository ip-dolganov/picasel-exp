from django_filters import rest_framework as filters


class WithActionFilterBackend(filters.DjangoFilterBackend):
    """
    Custom FilterBackend with action
    """

    def filter_queryset(self, request, queryset, view):
        filter_class = self.get_filter_class(view, queryset)
        if filter_class:
            return filter_class(request.query_params, queryset=queryset, request=request, action=view.action).qs
