from django.apps import apps
from rest_framework import permissions

PerModelEditToken = apps.get_model('edit_token', 'PerModelEditToken')
EditToken = apps.get_model('edit_token', 'EditToken')


class PerModelEditTokenPermissions(permissions.BasePermission):
    """
    Allow access with per-model token
    """

    def has_permission(self, request, view):
        edit_token = PerModelEditToken.objects.get_active_token(view.queryset.model.__name__, request.user)
        return edit_token is not None

    def has_object_permission(self, request, view, obj):
        edit_token = PerModelEditToken.objects.get_active_token(type(obj).__name__, request.user)
        return edit_token is not None


class EditTokenPermissions(permissions.BasePermission):
    """
    Allow access with token
    """

    def has_object_permission(self, request, view, obj):
        edit_token = EditToken.objects.get_active_token(obj, request.user)
        passed_token = request.META.get('HTTP_EDIT_TOKEN', None)
        return passed_token is not None and edit_token is not None and str(edit_token.uid) == passed_token
