from django.urls import include, re_path

app_name = 'api'


urlpatterns = [
    re_path(r'users/', include('users.urls', namespace='users')),
    re_path(r'tasks/', include('tasks_system.urls', namespace='tasks')),
]
