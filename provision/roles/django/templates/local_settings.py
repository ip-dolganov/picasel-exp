import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


DEBUG = {% if debug_mode | default("true") | bool %}True{% else %}False{% endif %}

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [{{ domains_django }}]


{% if not debug_mode | default("true") | bool %}PREPEND_WWW = True{% endif %}


# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ pg_db_name }}',
        'USER': '{{ pg_db_user }}',
        'PASSWORD': '{{ pg_db_password }}'
    }
}

# Static
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, '{{ static_path_rel }}')

DEBUG_STATICS = (os.path.join(BASE_DIR, '{{ src_static_path_rel }}'),
                 os.path.join(BASE_DIR, '{{ uncollected_static_rel }}'))

PROD_STATICS = (os.path.join(BASE_DIR, '{{ uncollected_static_rel }}'),)

STATICFILES_DIRS = DEBUG_STATICS if DEBUG else PROD_STATICS

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

MEDIA_ROOT = '{{ media_path }}'
TEST_MEDIA_ROOT = '{{ test_media_path }}'
MEDIA_URL = '/media/'

FIXTURE_DIRS = []


# Local Email Backend
EMAIL_BACKEND = '{% if vagrant_mode | default("false") | bool %}django.core.mail.backends.filebased.EmailBackend{% else %}django.core.mail.backends.smtp.EmailBackend{% endif %}'
EMAIL_FILE_PATH = os.path.join(BASE_DIR, 'mail')


# Project's domain
PROJECT_DOMAIN = '{{ site_domain }}'


HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': '{{ service_name }}',
        'EXCLUDED_INDEXES': ['pid_books.search_indexes.BookTitlesIndex']
    },
    'autocomplete': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': '{{ service_name }}_autocomplete',
        'EXCLUDED_INDEXES': []
    },
}

KEEN_PROJECT_ID = '589486358db53dfda8a8652c'
KEEN_WRITE_KEY = '087294C426B14ECDBCCB22BE417A6DA52F1167998F37CB78D4ECAAEB67F96A2F'


CHECK_EMAIL_EXISTS = {% if check_email | default("true") | bool %}True{% else %}False{% endif %}

# Celery
CELERY_BROKER_URL = 'amqp://{{ rabbitmq_user }}:{{ rabbitmq_password }}@localhost:5672/{{ rabbitmq_vhost }}'
CELERY_RESULT_BACKEND = 'amqp://{{ rabbitmq_user }}:{{ rabbitmq_password }}@localhost:5672/{{ rabbitmq_vhost }}'

__all__ = ['DATABASES', 'DEBUG', 'STATIC_URL', 'STATIC_ROOT', 'STATICFILES_DIRS', 'CHECK_EMAIL_EXISTS',
           'STATICFILES_FINDERS', 'MEDIA_ROOT', 'MEDIA_URL', 'EMAIL_BACKEND', 'EMAIL_FILE_PATH',
           'PROJECT_DOMAIN', 'TEST_MEDIA_ROOT', 'HAYSTACK_CONNECTIONS', 'KEEN_PROJECT_ID', 'KEEN_WRITE_KEY',
           'CELERY_BROKER_URL', 'CELERY_RESULT_BACKEND',
           'ALLOWED_HOSTS'{% if not debug_mode | default("true") | bool %}, 'PREPEND_WWW'{% endif %}]
