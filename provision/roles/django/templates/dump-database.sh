#!/usr/bin/env bash

sudo su -c 'pg_dump {{ pg_db_name }} -f "/var/lib/postgresql/{{ pg_db_name }}.dump.sql"' postgres
sudo cp /var/lib/postgresql/{{ pg_db_name }}.dump.sql {{ project_path }}/{{ pg_db_name }}.dump.sql
sudo chown {{ default_user }}:{{ default_user }} {{ project_path }}/{{ pg_db_name }}.dump.sql
sed 's=/vagrant/=/home/{{ default_user }}/{{ mainapp_name }}/=g' ./{{ pg_db_name }}.dump.sql > ./{{ pg_db_name }}_dev.dump.sql
tar -zcvf {{ project_path }}/media_files.tar.gz -C {{ media_path }}/ .