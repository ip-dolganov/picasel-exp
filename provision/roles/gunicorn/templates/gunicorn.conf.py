import multiprocessing

workers = multiprocessing.cpu_count() + int(multiprocessing.cpu_count() / 2)
reload = {% if vagrant_mode | default("true") | bool %}True{% else %}False{% endif %}

pid = '{{ pid_dir }}/pid'
timeout = 700
accesslog = '{{ log_dir }}/server_access.log'
errorlog = '{{ log_dir }}/server_error.log'
user = '{{ default_user }}'
umask = 755
worker_class = 'gunicorn.workers.ggevent.GeventWorker'
worker_connections = 10000
